#include <iostream>

using namespace std;

void mostrar(int array[][10], int Tamano_Matriz);
int comprobar(int array[][10], int Tamano_Matriz);

int main()
{
    int Tamano_Matriz, number;
    cout << "Ingrese n de una Matriz cuadrada: "; //Define tamaño cuadrado de la matriz
    cin >> Tamano_Matriz;
    int matriz[10][10];
    cout << "Digite numeros por posicion: \n";
    for(int i = 0; i < Tamano_Matriz; i++){   //Recorre matriz y digita cada numero en cada posicion correspondiente
        for(int j = 0; j < Tamano_Matriz; j++){
            cout << "Posicion " << "["<<i+1<<"]"<<"["<<j+1<<"]: ";
            cin >> number;
            matriz[i][j] = number;
        }
    }
    mostrar(matriz, Tamano_Matriz);
    if(comprobar(matriz, Tamano_Matriz)==1){
        cout << "Es un cuadrado magico\n";
    }
    else  cout << "No es cuadrado Magico\n";
    return 0;
}

void mostrar(int array[][10], int Tamano_Matriz){
    cout << endl;
    for(int i =0; i<Tamano_Matriz; i++){  //Imprime la matriz
        for(int j = 0; j<Tamano_Matriz; j++){
            cout << array[i][j]<< "  ";
        }
        cout << endl;
        cout << endl;
    }
    cout << endl;
}

int comprobar(int array[][10], int Tamano_Matriz){
    int aux =0, cont=0, suma, suma2,suma3;

    for(int i=0; i< Tamano_Matriz; i++){        //Condicion para recorrer todo el arreglo para la suma de sus filas, columnas y diagonales
        suma=0;  //Suma de filas
        suma2=0;    //SUma de columnas
        suma3=0;    //Suma de diagonales
        for(int j=0; j<Tamano_Matriz; j++){
            suma += array[i][j];
            suma2 += array[j][i];
            suma3 += array[j][j];
         }
        if(aux!=0){
            if(suma==suma2){ //Condicion para que las dos primeras sumas sean iguales y no sean cero
                if(aux == suma){
                    aux = suma;
                    cont++;
                }
            }
        }else if(suma==suma2 && suma2==suma3) // COndicion para que las 3 sumas sean iguales y por ende sea magico
                aux = suma;
    }

    if(cont== Tamano_Matriz-1) return 1;
    else return 0;
}
